from pathlib import Path
import os, logging
import pandas as pd
from numerous.image_tools.job import NumerousBaseJob, ExitCode
from numerous.html_report_generator import Report, Section, GoFigure, DataFrameTable
from numerous.image_tools.app import run_job

logger = logging.getLogger('MyHTMLReportJob')


class NumerousReportJob(NumerousBaseJob):

    def run_job(self) -> ExitCode:
        # Define output folder and html file
        folder = Path('.')
        filename = 'report'
        file = folder.joinpath(filename + '.html')

        # Remove previous report file if exists
        if os.path.exists(file):
            os.remove(file)

        # Create report
        report = Report(target_folder=folder, filename=filename)

        self.make_report(report)

        # Save the report - creates the html output file
        report.save()

        path_str = "./" + filename + ".html"
        with open(path_str, "r") as f:
            f.read()
        self.app.client.upload_file(path_str, filename)

        return ExitCode.COMPLETED

    def make_report(self, report: Report):
        raise NotImplementedError("Please implement the make_report method in your report job class")


class MyHTMLReportJob(NumerousReportJob):

    def make_report(self, report: Report):
        # Add info for the header and title page
        report.add_header_info(header='My Organization',
                               title='My First Report',
                               sub_title='but not the last',
                               sub_sub_title='at all',
                               footer_title='My first footer',
                               footer_content='This is the end!'
                               )

        # Add a section
        section = Section(section_title="Section 1")

        # create a figure
        figure = GoFigure({
            "data": [{"type": "bar",
                      "x": [1, 2, 3],
                      "y": [1, 3, 2]}],
            "layout": {"title": {"text": "A Figure Specified By Python Dictionary"}}
        }, caption="Test figure", notes=["Please notice x", "Please notice y"])

        # create a table
        table = DataFrameTable(pd.DataFrame({
            'a': [1, 2, 3],
            'b': [4, 5, 6]
        }), caption="This is a test table")

        # Add the figure and table to the section
        section.add_content({
            'figure': figure,
            'table': table
        })

        # Add the section to the report
        report.add_blocks({
            'section': section
        })


def run_example():
    run_job(numerous_job=MyHTMLReportJob(), appname="MyHTMLReportJob", model_folder=".")


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    run_example()
